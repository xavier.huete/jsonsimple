package com.xavi.jsonsimple;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class EscriureJSONprova {

    public static void main(String[] args) {

        JSONObject obj = new JSONObject();
        JSONArray list = new JSONArray();
        
        
        
        try (FileWriter file = new FileWriter("ExerciciJson")) {
        	String id = (String) obj.put("_id", "58c59c7499d4ee0af9e224e3");
        	String title = (String) obj.put("title", "Going Overboard");
        	String year = (String) obj.put("year", 1989);
        	String imdbId = (String) obj.put("imdbId", "tt0096870");
        	String mpaaRating = (String) obj.put("mpaaRating", "R");
        	String genre = (String) obj.put("genre", "Comedy");
        	Long viewerRating = (Long)  obj.put("viewerRating", 10);
        	Long viewerVotes = (Long) obj.put("viewerVotes", 10019);
        	Long runtime = (Long) obj.put("runtime", 99);
        	String director = (String) obj.put("director", "Valerie Breiman");
        	JSONArray cast = (JSONArray) obj.put("cast", list);
        		list.add("Burt Young");
        		list.add("Nicolas Cage");
        		list.add("Scott LaRose");
        		list.add("Lisa Collins");
        	String plot = (String) obj.put("plot", "A struggling young comedian takes a menial job on a cruise ship where ...");
        	String language = (String) obj.put("language", "English");
            
        	file.write(obj.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);

    }

}