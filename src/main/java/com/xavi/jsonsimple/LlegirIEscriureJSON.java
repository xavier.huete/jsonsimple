package com.xavi.jsonsimple;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Iterable;

public class LlegirIEscriureJSON {

	public static void main(String[] args) {

		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader("ExerciciJson"));

			JSONObject jsonObject = (JSONObject) obj;

			String id = (String) jsonObject.get("_id");
			System.out.println(id);

			String title = (String) jsonObject.get("title");
			System.out.println(title);

			Long year = (Long) jsonObject.get("year");
			System.out.println(year);

			String imdbId = (String) jsonObject.get("imdbId");
			System.out.println(imdbId);

			String mpaaRating = (String) jsonObject.get("mpaaRating");
			System.out.println(mpaaRating);

			String genre = (String) jsonObject.get("genre");
			System.out.println(genre);

			double viewerRating = (double) jsonObject.get("viewerRating");
			System.out.println(viewerRating);

			Long viewerVotes = (Long) jsonObject.get("viewerVotes");
			System.out.println(viewerVotes);

			Long runtime = (Long) jsonObject.get("runtime");
			System.out.println(runtime);

			String director = (String) jsonObject.get("director");
			System.out.println(director);

			// loop array
			JSONArray cast = (JSONArray) jsonObject.get("cast");
					
			for (Object s : cast) {
				System.out.println(s);
			}

			String plot = (String) jsonObject.get("plot");
			System.out.println(plot);

			String language = (String) jsonObject.get("language");
			System.out.println(language);

			try (FileWriter file = new FileWriter("ExerciciJson")) {
				jsonObject.put("_id", jsonObject.get("_id"));
				jsonObject.put("title", title);
				jsonObject.put("year", year);
				jsonObject.put("imdbId", imdbId);
				jsonObject.put("mpaaRating", mpaaRating);
				jsonObject.put("genre", genre);
				jsonObject.put("viewerRating", 10.0);
				jsonObject.put("viewerVotes", viewerVotes);
				jsonObject.put("runtime", runtime);
				jsonObject.put("director", director);
				jsonObject.put("cast", cast);
				if (cast.contains("Adam Sandler")) {
					cast.remove("Adam Sandler");
					cast.add("Nicolas Cage");
				}
				jsonObject.put("plot", plot);
				jsonObject.put("language", language);

				file.write(jsonObject.toJSONString());
				file.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			System.out.print(jsonObject);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

}